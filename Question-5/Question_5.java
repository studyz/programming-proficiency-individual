// ************************************************************
// Q5.java
//check
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Write a program that calculates and prints the sum and product
// of all the digits in an integer value read from the keyboard.
// ************************************************************

import java.util.*;
public class Question_5 {
	public static void main(String[] args) {
		long num; 
		int sum=0;
		String str_num;

		Scanner scan = new Scanner(System.in);
		System.out.println("Now please enter a number: ");
		try{
			num = scan.nextLong();
			int[]digits = Long.toString(num).chars().map(c -> c-='0').toArray();
			for(int d : digits)
					sum += d;
			System.out.println("The sum of all the digits in your input integer is: " + sum);
		}catch(Exception e){
			System.out.println("Warning: invalid inputs! Please enter integer numbers!");
		}
	}
}
