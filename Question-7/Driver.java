import java.util.*;
public class Driver{
  // arguments are passed using the text field below this editor
  public static void main(String[] args){
    Random random_generator = new Random();
    int count_times = random_generator.nextInt(9999) + 999999;
    double rollCount = 0, count2 = 0;    // Number of times the dice have been rolled.
    PairofDice dice = new PairofDice();  // Create the PairOfDice object.

    while (true){
      dice.roll();
      if(dice.getDifferent() == 2){
        count2++;
      }
      rollCount++;

      if (rollCount == count_times){
        System.out.println("Thr Odd is: " + String.format("%.3f", count2/rollCount));
        break;
      }
    }
  }
}
