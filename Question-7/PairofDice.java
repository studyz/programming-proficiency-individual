// ************************************************************
// Quesion_7 PairofDice.java
//
// Write by xueyang SID 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Using the Die class defined in Chapter 4, write a class called PairofDice, composed of two Die objects.
// Include methods to set and get the individual die values,
// a method to roll the dice and a method that returns the current difference of the twodie values.
// ************************************************************
import java.util.*;
public class PairofDice {
  public int die1;   // Number of the first die.
  public int die2;   // Number of the second die.

  public PairofDice() {
    roll();
  }

  public void set_PairofDice() {
    die1 = (int)(Math.random()*6) + 1;
    die2 = (int)(Math.random()*6) + 1;
  }

  public int get_die1() {
    return die1;
  }
  public int get_die2() {
    return die2;
  }
  public int getDifferent(){
    return Math.abs(die1-die2);
  }
  public void roll() {
    set_PairofDice();
  }
}
