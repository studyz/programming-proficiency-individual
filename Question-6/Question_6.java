// ************************************************************
// Question_6.java
// ??? and display their values before the program ends.
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Use a while loop, a for loop and a do-while loop to calculate separately the following sum
// sin(3 + 4/(5*6) + sin(4 + 5/(6*7)) + sin(5 + 6/(7*8)) + ... + sin(96 + 97/(98*99))
// Store the results in 3 separate variables and display their values before the program ends.
// ************************************************************
public class Question_6 {
	public static void main(String[] args) {
		double while_loop_result = 0.0, for_loop_result = 0.0, dowhile_loop_result = 0.0, count = 3.0;

		//while loop
		while (count <= 96.0){
			while_loop_result += Math.sin(count + (count +1) / ((count+2) * (count+3)));
			count ++;
		}
		System.out.println("while loop result is:\t" + while_loop_result);

		// for loop
		for (count = 3;count<=96;count++) {
			for_loop_result += Math.sin(count + (count +1) / ((count+2)*(count+3)));
			double count2 = count - 3;
		}
		System.out.println("for loop result is:\t" + for_loop_result);

		// do-while loop
		count = 3;
		do{
			dowhile_loop_result += Math.sin(count + (count +1) / ((count+2) * (count+3)));
			count ++;
		}while(count <= 96);
		System.out.println("dowhile loop result is:\t" + dowhile_loop_result);
	 }
}
