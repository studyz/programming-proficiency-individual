// ************************************************************
// Question3.java
//check!
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Write a program that generates a random integer base (b), height (h)
// for a triangle in the range of 10 to 30, exclusive,
// and then computes the area of the triangle:
// ************************************************************
import java.util.Random;
public class Question_3{
	public static void main(String[] args) {
		int base,height;
		Random randomGenerator = new Random();
		base = randomGenerator.nextInt(30-10+1)+10;
		height = randomGenerator.nextInt(30-10+1)+10;
		
		float area = (float)base*(float)height/2;
		System.out.println("Base: " + base + "\nHeight: " + height + "\n------------\nArea: " + area);
	 }
}
