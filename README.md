# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Individual Assignment 3010038 – Programming Proficiency
* Due: 11:59pm Friday the 14th of April 2017 (Week 7)

### Question 1 (PP 1.9, p. 81) – 2 marks
* Write a program that prints the following hour glass shape. Don’t print any unneeded characters. (That is, don’t make any character string longer than it has to be)
* #########
*  #######
*   #####
*    ###
*     #
*    ###
*   #####
*  #######
* #########

### Question 2 (PP 2.10, p. 135) – 3 marks
* Write a program that determines the value of the coins in a jar and prints the total in dollars and cents. Read with suitable prompts for the integer values that represent the number of quarters, dimes, nickels and pennies. (You can use this site to determine the value of each http://www.enchantedlearning.com/math/money/coins/)

### Question 3 (PP 3.9, p.184, modified) – 3 marks
* Write a program that generates a random integer base (b), height (h) for a triangle in the range of 10 to 30, exclusive, and then computes the area of the triangle:
* Area = base x height ÷ 2
* For this you must print the random integers in addition to the final area. For instance
* Base: 12
* Height: 26
* ------------
* Area: 156

### Question 4 (PP 4.6, p.229) – 3 marks
* Write a class called Shelf that contains instance data that represents the length, breadth, and capacity of the shelf. 
* Also include a boolean variable called occupied as instance data that represents whether the shelf is occupied or not. 
* Define the Shelf constructor to accept and initialize the length, breadth, and capacity of the shelf. 
* Each newly created Shelf is vacant (the constructor should initialize occupied to false). 
* Include getter and setter methods for all instance data. 
* Include a toString method that returns a one-line description of the shelf. 
* Create a driver class called ShelfCheck, whose main method instantiates and updates several Shelf objects.

### Questions 5 (PP 5.3, p.289) – 3 marks
* Write a program that calculates and prints the sum and product of all the digits in an integer value read from the keyboard.

### Question 6 – 3 marks
* Use a while loop, a for loop and a do-while loop to calculate separately the following sum
* sin(3 + 4/(5*6) + sin(4 + 5/(6*7)) + sin(5 + 6/(7*8)) + ... + sin(96 + 97/(98*99))
* Store the results in 3 separate variables and display their values before the program ends.

### Questions 7 (PP 4.9, p229 modified) - 3 marks
* Using the Die class defined in Chapter 4, write a class called PairofDice, composed of two Die objects. 
* Include methods to set and get the individual die values, a method to roll the dice and a method that returns the current difference of the two die values. 
* Write a driver/main method to calculate the odds of getting 2 as the difference between a pair of dice, accurate to 3 significant digits.
* Hint:
* Keep throwing a pair of dice for a big number (N) of times, counting the times (M) of the difference being 
2. Then the odds are M/N. As the number N increases, more and more significant digits in the ratio M/N will be stabilised, and the stabilised digits are considered the accurate digits.

### Submission Details
* Create a directory structure that contains source files for each question with the root directory as your student ID like so:
* - 12345678
*    - Question-1
*    - Question-2
*    - Question-3
*    - Question-4
*    - Question-5
*    - Question-6
*    - Question-7
* You are to then ZIP this into an compressed ZIP archive. Name your ZIP file with your student ID and the assignment name like so 12345678-assignment1.zip 
* Upload this newly created ZIP vUWS under the relevant submission link on vUWS.