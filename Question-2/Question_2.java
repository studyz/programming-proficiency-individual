// ************************************************************
// Question 2
//check!!
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Write a program that determines the value of the coins in a jar and prints the total in dollars and cents.
// Read with suitable prompts for the integer values that represent the number of quarters, dimes, nickels and pennies.
// ************************************************************
import java.util.*;
public class Question_2{
  public static void main(String[] args) {
    try{
      int dollars, cents;
      // 1 quarter = 25 cents
      Scanner scan = new Scanner(System.in);
      System.out.print("Please enter the number of quarters:\t");
      int quarters = scan.nextInt();
      // 1 dime = 10 cents
      System.out.print("Please enter the number of dimes:\t");
      int dimes = scan.nextInt();
      // 1 nickel = 5 cents
      System.out.print("Please enter the number of nickels:\t");
      int nickels = scan.nextInt();
      // 1 penny = 1 cent
      System.out.print("Please enter the number of pennies:\t");
      int pennies = scan.nextInt();

      int totoal_cents = quarters * 25 + dimes * 10 + nickels * 5 + pennies;
      // 100 cents = 1 dollar
      dollars = totoal_cents / 100;
      cents = totoal_cents % 100;
      System.out.println("The total amount is " + dollars + " dollars and " + cents + " cents!");

    }catch(Exception e){
			System.out.println("Warning: invalid inputs! Please input iteger!2");
		}

  }
}
