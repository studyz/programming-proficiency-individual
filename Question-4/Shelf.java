// ************************************************************
// Question 4 Shelf
//??? getter and setter
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Write a class called Shelf that contains instance data that represents the length,
// breadth, and capacity of the shelf. Also include a boolean variable called occupied
// as instance data that represents whether the shelf is occupied or not.
// Define the Shelf constructor to accept and initialize the length, breadth, and capacity of the shelf.
// Each newly created Shelf is vacant (the constructor should initialize occupied to false).
// Include getter and setter methods for all instance data. Include a toString method
// that returns a one-line description of the shelf.
// ************************************************************
public class Shelf{
  private int length, breadth, capacity;
  private boolean occupied;
//constractor
  public Shelf(int c_length,int c_breadth,int c_capacity){
    length = c_length;
    breadth = c_breadth;
    capacity = c_capacity;
    occupied = false;
    System.out.println("A new shelf is now created!\n");
  }
//occupied a shelf
  public void getter(){
    occupied = true;
    System.out.println("Shelf is now occupied!\n");
  }
//setting length breadth capacity by giving new numbers
  public void setter(int len, int bre, int cap){
    length = len;
    breadth = bre;
    capacity = cap;
    System.out.println("Shelf's setting is now changed!\n");
  }
//  print out current state
  public String toString(){
    String description = "This shelf's length, breadth, capacity and occupied are: " + length + ", "
      + breadth + ", " + capacity + ", " + occupied + ".\n";
    return description;
  }

}
