// ************************************************************
// Question 4 ShelfCheck
//??? getter and setter
// Write by xueyang sid 18942239 for
// Programming Proficiency Assigement 1;
// ************************************************************
// Create a driver class called ShelfCheck,
// whose main method instantiates and updates several Shelf objects
// ************************************************************
public class ShelfCheck{
  public static void main(String[] args) {
//    define a new shelf by giving the settings
    Shelf shelf = new Shelf(200,50,2000);
    System.out.println(shelf.toString());
//    set shelf with new length, breadth, capacity
    shelf.setter(100,20,1000);
//    occupied a shelf
    shelf.getter();
//    print out the state of the shelf
    System.out.println(shelf.toString());
  }
}
